//1.Сделать заглавным первый символ этой строки не используя цикл.//

//--перше рішення--//
let str = 'lesha'
let newStr = str[0].toUpperCase() + str.slice(1);

console.log(newStr)

//--друге рішення--//
let str1 = 'lesha'
function newStr1(str1) {
    if (!str1) return str1;

    return str1[0].toUpperCase() + str1.slice(1);
}

console.log(newStr1(str1))

//2.Даная строка, например '123456'. Переверните эту строку (сделайте
//из нее '654321') не используя цикл//

let str2 = '123456'
function reverseString(str2) {
    return str2.split("").reverse().join();
}

console.log(reverseString(str2))

//3.Проверить что строка начинаеться на http://. //

let find = 'http://getbootstrap.com/'
function myFunction(find) {
    let result
    result = find.startsWith('http://')
    return result
}
console.log(myFunction(find))

//4.Проверить что строка заканчиваеться на .html //

let find1 = "index.html"
function myFunction1(find1) {
    let result
    result = find1.endsWith('.html')
    return result
}

console.log(myFunction1(find1))


